" Title: vim-filed
" Description: A collection of custom settings based on filetypes
" Maintainer: Mike Murray
if exists("g:loaded_vimFiled")
    finish
endif
let g:loaded_vimFiled = 1
